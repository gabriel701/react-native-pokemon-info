import React from 'react';
import { View, StyleSheet, Platform, ViewStyle } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { useState, useEffect } from 'react';
import { useDebouncedValue } from '../hooks/useDebouncedValue';
import { SimplePokemon } from '../Interfaces/PokemonInterfaces';

interface Props {
  onDebaunce: (value: string) => void;
  style?: StyleProps<ViewStyle>;
}

export const SearchInput = ({ style, onDebaunce }: Props) => {
  const [textValue, settextValue] = useState('');
  const { debouncedValue } = useDebouncedValue(textValue);

  useEffect(() => {
    onDebaunce(debouncedValue);
  }, [debouncedValue]);

  return (
    <View
      style={{
        ...styles.container,
        ...(style as any),
      }}>
      <View style={styles.textBckGround}>
        <TextInput
          placeholder="Buscar Pokémon"
          style={{
            ...styles.textInput,
            top: Platform.OS === 'ios' ? 0 : 0,
          }}
          autoCapitalize="none"
          autoCorrect={false}
          value={textValue}
          onChangeText={settextValue}
        />
        <Icon name="search-outline" color="grey" size={30} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
  },
  textBckGround: {
    backgroundColor: '#F3F1F3',
    borderRadius: 50,
    height: 40,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  textInput: {
    flex: 1,
    fontSize: 18,
  },
});
