import React from 'react';
import { Image, ActivityIndicator, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { stylesApp } from '../Theme/AppTheme';
import { UsePokemonPagination } from '../hooks/UsePokemonPagination';
import { FlatList } from 'react-native-gesture-handler';
import { PokemonCard } from '../conponents/PokemonCard';

export const HomeScreen = () => {
  const { top } = useSafeAreaInsets();

  const { simplePokemonList, loadPokemons } = UsePokemonPagination();

  return (
    <>
      <Image
        source={require('../assets/pokebola.png')}
        style={stylesApp.pokebolaBG}
      />
      <View style={{ alignItems: 'center' }}>
        <FlatList
          data={simplePokemonList}
          keyExtractor={pokemon => pokemon.id}
          showsVerticalScrollIndicator={false}
          numColumns={2}
          ListHeaderComponent={
            <Text
              style={{
                ...stylesApp.title,
                ...stylesApp.globalMargin,
                top: top + 20,
                marginBottom: top + 20,
                paddingBottom: 10,
              }}>
              Pokedex
            </Text>
          }
          renderItem={({ item }) => <PokemonCard pokemon={item} />}
          // infinity Scroll
          onEndReached={loadPokemons}
          onEndReachedThreshold={0.4}
          ListFooterComponent={
            <ActivityIndicator style={{ height: 100 }} size={20} color="grey" />
          }
        />
      </View>
    </>
  );
};
