import React from 'react';
import { FlatList, Platform, Text, View, Dimensions } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { PokemonCard } from '../conponents/PokemonCard';
import { SearchInput } from '../conponents/SearchInput';
import { UsePokemonSearch } from '../hooks/UsePokemonSearch';
import { stylesApp } from '../Theme/AppTheme';
import { Loading } from '../conponents/Loading';
import { useState, useEffect } from 'react';
import { SimplePokemon } from '../Interfaces/PokemonInterfaces';

const screenWidth = Dimensions.get('window').width;

export const SearchScreen = () => {
  const { top } = useSafeAreaInsets();
  const { isFetching, simplePokemonList } = UsePokemonSearch();

  const [pokeFiltered, setPokeFiltered] = useState<SimplePokemon[]>([]);

  const [term, setTerm] = useState('');

  useEffect(() => {
    if (term.length === 0) {
      return setPokeFiltered([]);
    }

    if (isNaN(Number(term))) {
      setPokeFiltered(
        simplePokemonList.filter(poke =>
          poke.name.toLowerCase().includes(term.toLowerCase()),
        ),
      );
    } else {
      const pokemonById = simplePokemonList.find(poke => poke.id === term);
      setPokeFiltered(pokemonById ? [pokemonById] : []);
    }
  }, [term]);

  if (isFetching) {
    return <Loading />;
  }

  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 20,
      }}>
      <SearchInput
        onDebaunce={value => setTerm(value)}
        style={{
          position: 'absolute',
          zIndex: 999,
          width: screenWidth - 40,
          top: Platform.OS === 'ios' ? top : top + 10,
        }}
      />

      <FlatList
        data={pokeFiltered}
        keyExtractor={pokemon => pokemon.id}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        ListHeaderComponent={
          <Text
            style={{
              ...stylesApp.title,
              ...stylesApp.globalMargin,
              marginVertical: 10,
              marginTop: Platform.OS === 'ios' ? top + 45 : top + 50,
            }}>
            {term}
          </Text>
        }
        renderItem={({ item }) => <PokemonCard pokemon={item} />}
      />
    </View>
  );
};
