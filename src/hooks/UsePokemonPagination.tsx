import { useEffect, useRef, useState } from 'react';
import { PokemonApi } from '../api/PokemonApi';
import { Result } from '../Interfaces/PokemonInterfaces';
import {
  PokemonPaginatedResponse,
  SimplePokemon,
} from '../Interfaces/PokemonInterfaces';

export const UsePokemonPagination = () => {
  const [isLoading, setisLoading] = useState(true);

  const [simplePokemonList, setSimplePokemonList] = useState<SimplePokemon[]>(
    [],
  );

  const nextPageUrl = useRef(
    'https://pokeapi.co/api/v2/pokemon?limit=40https://pokeapi.co/api/v2/pokemon?limit=40',
  );

  const loadPokemons = async () => {
    setisLoading(true);
    const resp = await PokemonApi.get<PokemonPaginatedResponse>(
      nextPageUrl.current,
    );
    nextPageUrl.current = resp.data.next;
    mapPokemonList(resp.data.results);
  };

  const mapPokemonList = (pokemonList: Result[]) => {
    const newPokemonList: SimplePokemon[] = pokemonList.map(({ name, url }) => {
      const urlParts = url.split('/');
      const id = urlParts[urlParts.length - 2];
      const picture = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;

      return { id, picture, name };
    });
    setSimplePokemonList([...simplePokemonList, ...newPokemonList]);
    setisLoading(false);
  };

  useEffect(() => {
    loadPokemons();
  }, []);

  return {
    isLoading,
    simplePokemonList,
    loadPokemons,
  };
};
